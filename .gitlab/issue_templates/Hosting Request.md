## Description of the projects to be hosted

- Project Name:
- Project URL:
- Project Maintainers:
- Project License:

Please explain here how they meet our current criteria:

### Main criteria

- [ ] Free / Open Source
- [ ] related to Tryton
- [ ] main development repositories
- [ ] you are an official maintainer of the project
- [ ] The project adhere to the
      [*Tryton Community* rules](https://tryton.community/policy/)

### Additional information

Knowing this can help in case there are constraints on resources and/or
administrators availability at the time of the request. It can also help us
suggest the right solution for the needs of the project.

- is the project officially released?
  In downstream distributions or packaging systems?
- estimation of the user base if possible, or number of downloads,
  reverse dependencies…
- number of maintainers and regular contributors
- where is the project currently hosted? Are there issue trackers,
  review systems, CI/CD to hook to or to replace somehow?

### Hosting type

What would you like to have?

- [ ] a project/repository
- [ ] a group

in which area:

- [ ] modules
- [ ] documentation
- [ ] tools
- [ ] …


## Post approval maintainer task list

See [README](README.md#maintainer-task-list-for-the-import-to-happen)
for explanations.

- [ ] When using Mercurial, review the [Heptapod
      workflow](https://heptapod.net/pages/faq.html#workflow)


/label ~"Hosting Request"
