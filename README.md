tryton-community @ heptapod
===============================

Admissible projects
----------------------

All Free and Open Source projects related to Tryton
are welcome on tryton-community @ heptapod
as far as the current resources of heptapod permit.

If you want to bring in your project,
please [create an issue in this project](../../-/../issues/new)
with the "Hosting Request" template.
Upon approval, we will usually allow you to create what you need.

Note: to avoid confusing situations,
we require that the person requesting hosting
be an official maintainer of the project,
or to have been publicly asked to do so by a maintainer.


### Maintainer task list for the import to happen

Once the Hosting Request is approved,
it should be assigned to one of the
*Tryton community* administrators,
who will give the needed rights or create a group
with the proper ownership.
